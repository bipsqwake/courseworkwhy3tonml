package nml.Expr;

import nml.Getter;

import java.io.StringReader;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Set;

/**
 *   __
 * <(o )___
 * ( ._> /
 * `---'
 */
public class If extends Action {

    private Deque<IfBranch> branches = new ArrayDeque<>();

    private void AddBranch(IfBranch branch) {
        branches.addLast(branch);
    }

    private static boolean isBlockEnd(String input, String ... sample) {
        for (String a: sample)
            if (input.endsWith(a)) return true;
        return false;
    }

    private static String getBlock(Getter g) {
        StringBuilder block = new StringBuilder();
        int bracets = 0;
        while (!g.end() && (bracets > 0 || !isBlockEnd(block.toString(), " elif", "else"))) {
            block.append(g.getChar());
            if (block.toString().endsWith(" if")) bracets++;
            if (block.toString().endsWith(" endif;")) bracets--;
        }
        return block.toString();
    }

    public static If createIf(String input) {
        If ifStatement = new If();
        System.out.println("if block");
        Getter g = new Getter(new StringReader(input));
        boolean branches = true;
        boolean hasElse = false;
        while (branches) {
            StringBuilder condition = new StringBuilder();
            while (!g.end() && !condition.toString().endsWith(" then")) {
                condition.append(g.getChar());
            }
            if (g.end()) return null;
            condition.setLength(condition.length() - " then".length());
            String block = getBlock(g);
            if (isBlockEnd(block, " else")) {
                branches = false;
                hasElse = true;
            } else if (g.end()) {
                branches = false;
            }
            System.out.println("if inter block");
            Expr tmpCond = Expr.createExpr(condition.toString());
            Deque<Action> tmpBlock = Action.processBlock(block);
            if (tmpCond != null && tmpBlock != null) {
                ifStatement.AddBranch(new IfBranch(tmpBlock, tmpCond));
            } else return null;
        }
        if (hasElse) {
            String block = getBlock(g);
            System.out.println("else block");
            Deque<Action> tmpBlock = Action.processBlock(block);
            if (tmpBlock != null)
                ifStatement.AddBranch(new IfBranch(tmpBlock));
            else return null;
        }
        return ifStatement;
    }

    @Override
    public String translate(Set<String> writes, Set<String> rules, ProgHolder holder) {
        return null;
    }
}
