package nml.Expr;

import java.util.Deque;

/**
 * __
 * <(o )___
 * ( ._> /
 * `---'
 */
class IfBranch {
    boolean hasCondition = false;
    Expr condition;
    Deque<Action> block;
    IfBranch(Deque<Action> block) {
        this.block = block;
    }
    IfBranch(Deque<Action> block, Expr condition) {
        this.block = block;
        this.condition = condition;
        this.hasCondition = true;
    }
}