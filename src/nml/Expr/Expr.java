package nml.Expr;

import nml.Getter;
import nml.Log;

import java.io.StringReader;
import java.util.*;


/**
 *   __
 * <(o )___
 * ( ._> /
 * `---'
 */
public class Expr extends Action {

    Deque<String> poliz;

    public Deque<String> getPoliz() {
        return poliz;
    }

    public Expr(Deque<String> poliz) {
        this.poliz = poliz;
    }

    private static Deque<String> makePoliz(String input) {
        String spaced = input;
        Deque<String> result = new ArrayDeque<>();
        Deque<String> texas = new ArrayDeque<>();
        for (String a: Operations.Cupuch) {
            spaced = spaced.replaceAll(a, " " + a + " ");
        }
        Getter g = new Getter(new StringReader(spaced));
        String token;
        boolean unary = true;
        System.out.println(input);
        System.out.println(spaced);
        while (!g.end() && (token = g.getToken()) != null) {
            if (Operations.operations.contains(token)) {
                if (unary && token.equals("-")) {
                    token = "?";
                }
                while (!texas.isEmpty() && (Operations.priority(token) <= Operations.priority(texas.peekFirst()))) {
                    result.addFirst(texas.removeFirst());
                }
                unary = false;
                texas.addFirst(token);
            } else if (token.equals("[")) {
                unary = false;
                texas.addFirst(token);
            } else if (token.equals("]")) {
                while (!texas.isEmpty() && !texas.peekFirst().equals("[")) {
                    result.addFirst(texas.removeFirst());
                }
                unary = false;
                result.addFirst("get");
            } else if (token.equals("(")) {
                unary = true;
                texas.addFirst(token);
            } else if (token.equals(")")) {
                while (!texas.isEmpty() && !texas.peekFirst().equals("(")) {
                    result.addFirst(texas.removeFirst());
                }
                if (!texas.isEmpty())
                    texas.removeFirst();
                if (Operations.functions.contains(texas.peekFirst())) {
                    result.addFirst(texas.removeFirst());
                }
                unary = false;
            } else if (Operations.functions.contains(token)) {
                unary = false;
                texas.addFirst(token);
            } else {
                unary = false;
                result.addFirst(token);
            }
        }
        while (!texas.isEmpty()) {
            result.addFirst(texas.removeFirst());
        }
        return result;
    }

    public static Expr createExpr(String input) {
        if (input.contains(".action")) {
            Getter g = new Getter(new StringReader(input));
            String name = g.getLineTo('(', '.');
            Deque<String> poliz;
            if (g.lastChar() == '(') {
                g.pushBack('(');
                poliz = makePoliz(g.getBlock('(', ')'));
            } else {
                poliz = new ArrayDeque<>();
            }
            poliz.addFirst(name + ".action");
            Iterator<String> iterator = poliz.descendingIterator();
            while (iterator.hasNext()) {
                System.out.print(iterator.next() + "%");
            }
            System.out.println();
            return new Expr(poliz);
        }
        System.out.println("Expr");
        Deque<String> poliz = makePoliz(input);
        Iterator<String> iterator = poliz.descendingIterator();
        while (iterator.hasNext()) {
            System.out.print(iterator.next() + "%");
        }
        System.out.println();
        return new Expr(poliz);
    }

    @Override
    public String translate(Set<String> writes, Set<String> rules, ProgHolder holder, Deque<Argument> args) {
        Deque<String> result = new ArrayDeque<>();
        Iterator<String> iterator = poliz.descendingIterator();
        while (iterator.hasNext()) {
            String next = iterator.next();

        }
        return null;
    }
}
