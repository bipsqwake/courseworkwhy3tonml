package nml.Expr;

/**
 *   __
 * <(o )___
 * ( ._> /
 * `---'
 */
public class Translate {
    String string;
    int size;

    public String getString() {
        return string;
    }

    public int getSize() {
        return size;
    }

    public void setString(String string) {
        this.string = string;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public Translate(String string, int size) {
        this.string = string;
        this.size = size;
    }
}
