package nml.Expr;

import nml.Arguable;

/**
 *   __
 * <(o )___
 * ( ._> /
 * `---'
 */
public class Argument {
    private String name;
    private String transName;
    private Arguable type;

    public String getName() {
        return name;
    }

    public String getTransName() {
        return transName;
    }

    public Arguable getType() {
        return type;
    }

    public Argument(String name, Arguable type) {
        this.name = name;
        this.type = type;
        this.transName = name.toLowerCase();
    }
}
