package nml.Expr;

import nml.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *   __
 * <(o )___
 * ( ._> /
 * `---'
 */
public class Operations {
    public static List<String> Cupuch = new ArrayList<String>
            (Arrays.asList("\\+", "-", "\\*", "/", "\\^", "~", "<<", ">>", "::", "\\(", "\\)", "\\[", "\\]", ",", "<=", "==", ">="));
    public static List<String> operations = new ArrayList<String>
            (Arrays.asList("+", "-", "*", "/", "&", "|", "^", "~", "<<", ">>", "::", ",", "<=", ">=", "==", "=", "!="));
    public static List<String> functions = new ArrayList<String>
            (Arrays.asList("coerce", "cast", "card"));
    public static int priority(String operation) {
        switch (operation) {
            case "::":
                return 14;
            case "~":
            case "?":
                return 13;
            case "*":
            case "/":
                return 12;
            case "+":
            case "-":
                return 11;
            case "<<":
            case ">>":
                return 10;
            case ">":
            case "<":
            case ">=":
            case "<=":
                return 9;
            case "==":
            case "!=":
                return 8;
            case "&":
                return 7;
            case "^":
                return 6;
            case "|":
                return 5;
            case "&&":
                return 4;
            case "||":
                return 3;
            case "=":
                return 2;
            case ",":
                return 1;
            case "(":
                return 0;
            default:
                Log.d("Unknown operation " + operation);
                return 0;
        }
    }
}
