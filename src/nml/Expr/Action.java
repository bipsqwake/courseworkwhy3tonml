package nml.Expr;

import nml.Getter;

import java.io.StringReader;
import java.util.*;

/**
 *   __
 * <(o )___
 * (  ._> /
 * `-----'
 */

public abstract class Action {
    private static Action processKeyword(String input, Getter g) {
        if (input.equals("if")) {
            g.pushBack('f');
            g.pushBack('i');
            If result = If.createIf(g.getBlock("if", "endif"));
            g.getChar();
            return result;
        }
        return null;
    }

    private static Action processExpr(String input) {
        return Expr.createExpr(input);
    }

    public static Deque<Action> processBlock(String input) {
        Set<String> keywords = new HashSet<>(Arrays.asList("if"));
        Deque<Action> result = new ArrayDeque<>();
        String token;
        Getter g = new Getter(new StringReader(input));
        while (!g.end()) {
            token = g.getToken();
            if (token == null) break;
            if (keywords.contains(token)) {
                result.addLast(processKeyword(token, g));
            }
            else {
                g.pushBackToken(token);
                result.addLast(processExpr(g.getLineTo(';')));
            }
        }
        return result;
    }

    public abstract String translate(Set<String> writes, Set<String> rules, ProgHolder holder, Deque<Argument> args);
}
