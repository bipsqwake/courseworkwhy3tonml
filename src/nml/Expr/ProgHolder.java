package nml.Expr;

import nml.*;

import java.util.LinkedList;
import java.util.List;

/**
 * __
 * <(o )___
 * ( ._> /
 * `---'
 */

public class ProgHolder {
    public TypeList typeList = new TypeList();
    public List<Reg> regs = new LinkedList<>();
    public List<Mem> mems = new LinkedList<>();
    public List<Var> vars = new LinkedList<>();
    public List<Mode> modes = new LinkedList<>();
    public List<Op> ops = new LinkedList<>();
}