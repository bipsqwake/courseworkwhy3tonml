package nml;

/**
 * __
 * <(o )___
 * ( ._> /
 * `---'
 */
public class Counter {
    public static double eval(String input) {
        if (input.equals("1")) {
            return 1;
        }
        if (input.contains("**")) {
            String[] a = input.split("\\*\\*");
            a[0] = a[0].replaceAll(" ", "");
            a[1] = a[1].replaceAll(" ", "");
            return Math.pow(Integer.parseInt(a[0]), Integer.parseInt(a[1]));
        }
        return 0;
    }
}
