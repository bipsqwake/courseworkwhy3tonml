package nml;

import Why3.Putter;
import nml.Expr.*;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.*;

public class Model {
    Set<String> keywords = new HashSet<String>(Arrays.asList("type", "card", "int", "reg", "mem", "var", "mode",
            "op"));
    ProgHolder progHolder = new ProgHolder();
    StringBuilder expr = new StringBuilder();
    Getter g;
    char c;

    public void process(String filename) throws IOException {
        Reader fileReader = new FileReader(filename);
        g = new Getter(fileReader);
        while (!g.end()) {
            c = g.getChar();
            if (g.end()) return;
            if (Character.isWhitespace(c)) {
                if (keywords.contains(expr.toString())) {
                    procToken(expr.toString());
                }
                expr.setLength(0);
            } else {
                expr.append(c);
            }
        }
    }

    private void procToken(String input) throws IOException {
        if (input.equals("type")) {
            Type tmp = Type.createType(g);
            if (tmp != null) {
                progHolder.typeList.add(tmp);
            }
        }
        if (input.equals("reg")) {
            Reg tmp = Reg.createReg(progHolder, g);
            if (tmp != null)
                progHolder.regs.add(tmp);
        }
        if (input.equals("mem")) {
            Mem tmp = Mem.createMem(progHolder, g);
            if (tmp != null)
                progHolder.mems.add(tmp);
        }
        if (input.equals("var")) {
            Var tmp = Var.createVar(progHolder, g);
            if (tmp != null)
                progHolder.vars.add(tmp);
        }
        if (input.equals("mode")) {
            Mode tmp = Mode.createMode(progHolder, g);
            if (tmp != null)
                progHolder.modes.add(tmp);
        }
        if (input.equals("op")) {
            Op tmp = Op.createOp(progHolder, g);
            if (tmp != null)
                progHolder.ops.add(tmp);
        }
    }

    public void out() {
        System.out.println("Types");
        for (Type a: progHolder.typeList.getList()) {
            System.out.println(a.getName() + " " + a.getBvSize());
        }
        System.out.println("Regs");
        for (Reg a: progHolder.regs) {
            System.out.print(a.getName() + " ");
            switch(a.getType()) {
                case Reg.REG:
                    System.out.print("Reg ");
                    System.out.print("Type = " + a.getRegType().name);
                    break;
                case Reg.REG_SET:
                    System.out.print("Reg set ");
                    System.out.print("size = " + a.getSize() + " ");
                    System.out.print("Type = " + a.getRegType().name);
                    break;
                case Reg.ALIAS_TO_REG:
                    System.out.print("Alias to reg ");
                    System.out.print("Reg: " + a.getAlias().getName() + " ");
                    System.out.print("From " + a.getAliasFrom());
                    break;
                case Reg.ALIAS_TO_REG_PART:
                    System.out.print("Alias to reg part ");
                    System.out.print("Reg: " + a.getAlias().getName() + " ");
                    System.out.print("From " + a.getAliasFrom() + " to " + a.getAliasTo());
                    break;
            }
            System.out.println();
        }
        System.out.println("Mems");
        for (Mem a: progHolder.mems) {
            System.out.println(a.getName() + " size = " + a.getSize() + " type = " + a.getType().name);
        }
        System.out.println("Vars");
        for (Var a: progHolder.vars) {
            System.out.println(a.getName() + " type = " + a.getType().name);
        }
        System.out.println("Modes");
        for (Mode a: progHolder.modes) {
            System.out.println(a.getName() + " type = " + a.getInputType().name + " reg = " + a.getReg().getName());
        }
        System.out.println("Ops");
        for (Op a: progHolder.ops) {
            System.out.println(a.toString());
        }
    }

    public void translate(Putter p) {
        for (Type a : progHolder.typeList.getList()) {
            a.translate(p);
        }
        for (Reg a: progHolder.regs) {
            a.translate(p);
        }
        for (Mem a: progHolder.mems) {
            a.translate(p);
        }
        for (Var a: progHolder.vars) {
            a.translate(p);
        }
        for (Op a: progHolder.ops) {
            a.translate(p);
        }
    }


}
