package nml;

import nml.Expr.Action;
import nml.Expr.ProgHolder;

import java.util.Deque;

/**
 *   __
 * <(o )___
 * ( ._> /
 * `---'
 */
public class Mode implements Arguable{
    private String name;
    private Type inputType;
    private Reg reg;
    private boolean hasAction = false;
    private Deque<Action> action;

    public Mode(String name, Type inputType, Reg reg) {
        this.name = name;
        this.inputType = inputType;
        this.reg = reg;
    }

    public Mode(String name, Type inputType, Reg reg, Deque<Action> action) {
        this.name = name;
        this.inputType = inputType;
        this.reg = reg;
        hasAction = true;
        this.action = action;
    }

    public String getName() {
        return name;
    }

    @Override
    public String getTransName() {
        return inputType.transName;
    }

    @Override
    public boolean isTranslated() {
        return getInputType().translated;
    }

    public Type getInputType() {
        return inputType;
    }

    public Reg getReg() {
        return reg;
    }

    public static Mode createMode(ProgHolder holder, Getter g) {
        StringBuilder name = new StringBuilder();
        StringBuilder input = new StringBuilder();
        StringBuilder reg = new StringBuilder();
        char c;
        g.eraseWhiteSpacesInLine();
        while ((c = g.getChar()) != '(' && !Character.isWhitespace(c)) {
            name.append((char) c);
            //c = g.getChar();
        }
        g.eraseWhiteSpacesInLine();
        g.getChar();
        int bracets = 0;
        while ((c = g.getChar()) != ')' || bracets > 0) {
            if (c == '(') bracets++;
            if (c == ')') bracets--;
            input.append((char) c);
        }
        g.eraseWhiteSpacesInLine();
        if (!g.nextName("=")) {
            return null;
        }
        g.eraseWhiteSpacesInLine();
        while ((c = g.getChar()) != '\n') {
            reg.append(c);
        }
        String[] type_tokens = input.toString().split(" ");
        Type tmpType = holder.typeList.getByName(type_tokens[1]);
        String[] reg_tokens = reg.toString().split("\\[");
        Reg tmpReg = null;
        for (Reg a: holder.regs) {
            if (a.getName().equals(reg_tokens[0])) {
                tmpReg = a;
            }
        }
        if (tmpReg == null) {
            return null;
        }
        g.getLineTo('\n');
        g.getLineTo('\n');
        if (g.nextName("action")) {
            Deque<Action> action = Action.processBlock(g.getBlock('{', '}'));
            return new Mode(name.toString(), tmpType, tmpReg, action);
        } else
            return new Mode(name.toString(), tmpType, tmpReg);
    }
}
