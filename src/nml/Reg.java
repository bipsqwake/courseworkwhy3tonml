package nml;

import Why3.Putter;
import nml.Expr.ProgHolder;

import java.io.IOException;

public class Reg implements Arguable{
    public static final int REG = 0;
    public static final int REG_SET = 1;
    public static final int ALIAS_TO_REG = 2;
    public static final int ALIAS_TO_REG_PART = 3;

    private String name;
    private String transName;
    private boolean translated = false;
    private int type;
    private Type reg_type;
    //Alias things
    private Reg alias;
    private int aliasFrom;
    private int aliasTo;
    //Reg_set
    private int size;

    public Reg(String name, Type reg_type, int size) {
        this.name = name;
        this.reg_type = reg_type;
        this.size = size;
        if (size == 1) {
            this.type = REG;
        } else {
            this.type = REG_SET;
        }
    }
    public Reg(String name, int type, Type reg_type, Reg alias, int aliasFrom, int aliasTo) {
        this.name = name;
        this.type = type;
        this.reg_type = reg_type;
        this.alias = alias;
        this.aliasFrom = aliasFrom;
        this.aliasTo = aliasTo;
    }
    public String getName() {return name;}

    @Override
    public String getTransName() {return transName;}

    @Override
    public boolean isTranslated() {
        return translated;
    }

    public int getType() {return type;}

    public Type getRegType() {return reg_type;}

    public Reg getAlias() {return alias;}
    public int getAliasFrom() {return aliasFrom;}
    public int getAliasTo() {return aliasTo;}

    public int getSize() {return size;}


    public static Reg createReg(ProgHolder holder, Getter g) throws IOException {
        StringBuilder name = new StringBuilder();
        StringBuilder type = new StringBuilder();
        char c;
        do {                    //get name
            c = g.getChar();
        } while (Character.isWhitespace((char) c));
        while (c != '[' && !Character.isWhitespace(c)) {
            name.append((char) c);
            c = g.getChar();
        }
        while (Character.isWhitespace(c)) {
            c = g.getChar();
        }
        while (c != ']') {
            type.append((char) c);
            c = g.getChar();
        }
        type.append((char) c);
        String[] type_tokens = type.toString().split("[\\[\\], ]");
        Type tmpType = null;
        int size = 1;
        if (type_tokens.length == 2) {
            tmpType = holder.typeList.getByName(type_tokens[1]);
        } else if (type_tokens.length == 4) {
            tmpType = holder.typeList.getByName(type_tokens[3]);
            size = Integer.parseInt(type_tokens[1]);
        }
        g.eraseWhiteSpacesInLine();
        if ((c = g.getChar()) == '\n') {
            return new Reg(name.toString(), tmpType, size);
        }
        g.pushBack(c);
        if (g.nextName("alias")) {
            while (c != '=') {
                c = g.getChar();
            }
            g.eraseWhiteSpacesInLine();
            StringBuilder aliasName = new StringBuilder();
            c = g.getChar();
            while (c != '<' && c != '[') {
                aliasName.append(c);
                c = g.getChar();
            }
            int aliasType = (c == '<') ? ALIAS_TO_REG_PART : ALIAS_TO_REG;
            StringBuilder borders = new StringBuilder();
            c = g.getChar();
            while (c != '>' && c != ']') {
                borders.append(c);
                c = g.getChar();
            }
            String[] border = borders.toString().split("\\.\\.");
            Reg aliasReg = null;
            for (Reg a: holder.regs) {
                if (a.getName().equals(aliasName.toString())) {
                    aliasReg = a;
                }
            }
            if (aliasReg == null) {
                throw new RuntimeException("Unexpected " + aliasName.toString());
            }
            if (border.length == 1) {
                return new Reg(name.toString(), aliasType, tmpType, aliasReg, Integer.parseInt(border[0]), Integer.parseInt(border[0]));
            } else if (border.length == 2) {
                return new Reg(name.toString(), aliasType, tmpType, aliasReg, Integer.parseInt(border[0]), Integer.parseInt(border[1]));
            }
        }
        return null;
    }

    public void translate(Putter p) {
        transName = name.toLowerCase();
        StringBuilder result = new StringBuilder();
        if (type == ALIAS_TO_REG || type == ALIAS_TO_REG_PART) {
            return;
        }
        if (!reg_type.translated) {
            Log.e("Cant translate " + name + " cause of not translated type");
        }
        if (type == REG) {
            result.append("val ").append(transName).append(" : ref ").append(reg_type.transName);
        } else {
            result.append("type ").append(transName).append("_type").append(" = {mutable ");
            result.append(transName).append("_regs : array ").append(reg_type.transName).append("} ");
            result.append("invariant { length self.").append(transName).append("_regs = ").append(size).append("}\n");
            result.append("val ").append(transName).append(" : ").append(transName).append("_type");
        }
        translated = true;
        p.write(result.toString());
    }
}
