package nml;

import java.io.*;
import java.util.Stack;


public class Getter {
    private Reader reader;
    private Stack<Character> rets = new Stack<>();
    private boolean end;
    private char c;


    public Reader getReader() {
        return reader;
    }

    public Getter(Reader reader) {
        this.reader = reader;
        end = false;
    }

    public char getChar() {
        if (!rets.empty()) {
            return rets.pop();
        }
        if (end) {
            return 0;
        }
        try {
            int t = reader.read();
            if (t == -1) {
                end = true;
                return '\n';
            }
            c = (char) t;
            if (c == '/') {
                t = reader.read();
                if (t == -1) {
                    throw new RuntimeException("unexpected /");
                }
                c = (char) t;
                if (c == '/') {
                    while (c != '\n') {
                        t = reader.read();
                        if (t == -1) {
                            end = true;
                            return '\n';
                        }
                        c = (char) t;
                    }
                } else if (c == '*') {
                    boolean comment = true;
                    while (comment) {
                        t = reader.read();
                        if (t == -1) {
                            throw new RuntimeException("unexpected /*");
                        }
                        c = (char) t;
                        if (c == '*') {
                            t = reader.read();
                            if (t == -1) {
                                throw new RuntimeException("unexpected /*");
                            }
                            c = (char) t;
                            if (c == '/') comment = false;
                        }
                    }
                    t = reader.read();
                    if (t == -1) {
                        end = true;
                        return 0;
                    }
                    c = (char) t;
                } else {
                    pushBack(c);
                    c = '/';
                }
            }
        } catch (IOException e) {
            throw new RuntimeException("read error");
        }
        return c;

    }

    public void pushBack(char a) {rets.push(a);}

    public void pushBackToken(String a) {
        for (int i = a.length() - 1; i >= 0; i--) {
            pushBack(a.charAt(i));
        }
    }

    public void eraseWhiteSpacesInLine() {
        do {
            c = getChar();
        } while (!end && Character.isWhitespace(c) && c != '\n');
        pushBack(c);
    }

    public void eraseWhiteSpaces() {
        do {
            c = getChar();
        } while (!end && Character.isWhitespace(c));
        pushBack(c);
    }

    public boolean nextName(String toFind) {
        String token = getToken();
        if (token.equals(toFind)) {
            return true;
        } else {
            for (int i = token.length() - 1; i >= 0; i--) {
                pushBack(token.charAt(i));
            }
            return false;
        }
    }

    public void goToToken(String toFind) {
        StringBuilder token = new StringBuilder();
        while (!end && !token.toString().equals(toFind)) {
            token.setLength(0);
            eraseWhiteSpacesInLine();
            c = getChar();
            while (!Character.isWhitespace(c)) {
                token.append(c);
                c = getChar();
            }
        }
        pushBack(c);
    }

    public String getBlock(char start, char ending) {
        StringBuilder result = new StringBuilder();
        c = getChar();
        while (!end && (c != start)) {
            c = getChar();
        }
        while (!end && (c != ending)) {
            c = getChar();
            if (c != ending)
                result.append(c);
        }
        return result.toString();
    }

    public String getBlock(String start, String end) {
        StringBuilder result = new StringBuilder();
        while (!getToken().equals(start));
        int bracets = 1;
        while (bracets > 0) {
            result.append(getChar());
            if (result.toString().endsWith(" " + start)) bracets++;
            if (result.toString().endsWith(end)) bracets--;
        }
        result.setLength(result.length() - end.length());
        return result.toString();
    }

    public String getToken() {
        StringBuilder result = new StringBuilder();
        eraseWhiteSpaces();
        if (end) return null;
        do {
            c = getChar();
            if (!end  && !Character.isWhitespace(c)) {
                result.append(c);
            }
        } while (!Character.isWhitespace(c));
        pushBack(c);
        return result.toString();
    }

    public String getLineTo(char ... ending) {
        StringBuilder result = new StringBuilder();
        boolean stop = false;
        while (!end && !stop) {
            c = getChar();
            for (char a: ending) {
                if (c == a) {
                    stop = true;
                }
            }
            if (!stop)
                result.append(c);
        }
        return result.toString();
    }

    public char lastChar() {
        return c;
    }

    public boolean end() {
        return end;
    }

}
