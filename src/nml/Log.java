package nml;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * __
 * <(o )___
 * ( ._> /
 * `---'
 */
public class Log {
    private static FileWriter writer;

    public static void init() {
        try {
            writer = new FileWriter("log.txt", false);
        } catch (IOException e) {
            System.out.println("Can't init log");
        }
    }

    public static void d(String string) {
        try {
            writer.write("Debug: " + string + "\n");
            writer.flush();
        } catch (IOException e) {
            System.out.println("LAL");
        }
    }

    public static void e(String string) {
        try {
            writer.write("Error: " + string + "\n");
            writer.flush();
        } catch (IOException e) {
            System.out.println("CANT LOG ERROR LOL");
        }
    }
}
