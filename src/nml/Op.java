package nml;

import Why3.Putter;
import nml.Expr.*;

import java.util.*;

/**
 *   __
 * <(o )___
 * ( ._> /
 * `---'
 */
public class Op {
    private String name;
    private String transName;
    private Deque<Argument> args;
    private Deque<Action> init;
    private Deque<Action> action;

    public Op(String name, Deque<Argument> args, Deque<Action> action, Deque<Action> init) {
        this.name = name;
        this.args = args;
        this.action = action;
        this.init = init;
    }

    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append(name).append('\n');
        for (Argument a: args) {
            result.append(a.getName()).append(" ").append( a.getType().getName()).append(", ");
        }
        return result.toString();
    }

    //creating
    private static Deque<Argument> getArgs(String input, ProgHolder holder) {
        Deque<Argument> result = new ArrayDeque<>();
        String[] arg_tokens = input.split(",");
        for (int i = 0; i < arg_tokens.length; i++) {
            arg_tokens[i] = arg_tokens[i].replaceAll(" ", "");
        }
        for (String a: arg_tokens) {
            String[] arg = a.split(":");
            Arguable type = null;
            for (Type b:holder.typeList.getList()) {
                if (b.name.equals(arg[1])) {
                    type = b;
                }
            }
            for (Mode b:holder.modes) {
                if (b.getName().equals(arg[1])) {
                    type = b;
                }
            }
            if (type == null) {
                return null;
            }
            result.addLast(new Argument(arg[0], type));
        }
        return result;
    }

    public static Op createOp(ProgHolder holder, Getter g) {
        StringBuilder name = new StringBuilder();
        StringBuilder args = new StringBuilder();
        Deque<Action> init;
        Deque<Action> action;
        char c;
        g.eraseWhiteSpacesInLine();
        while ((c = g.getChar()) != '(' && c != '=' && !Character.isWhitespace(c)) {
            name.append((char) c);
        }
        g.eraseWhiteSpacesInLine();
        if ((c = g.getChar()) == '=') {
            return null;
        }
        while ((c = g.getChar()) != ')') {
            args.append(c);
        }
        Deque<Argument> tmpArgs = getArgs(args.toString(), holder);
        if (tmpArgs == null) {
            return null;
        }
        g.goToToken("init");
        System.out.println(name.toString());
        init = Action.processBlock(g.getBlock('{', '}'));
        g.goToToken("action");
        action = Action.processBlock(g.getBlock('{', '}'));
        return new Op(name.toString(), tmpArgs, action, init);
    }

    public void translate(Putter p) {
        StringBuilder signature = new StringBuilder();
        transName = name.toLowerCase();
        signature.append("val ").append(transName).append(" ");
        for (Argument next : args) {
            if (!next.getType().isTranslated()) {
                Log.e("Can't translate " + name + "cause of " + next.getType().getName() + " not translated");
                return;
            }
            signature.append("(").append(next.getTransName()).append(" : ").append(next.getType().getTransName()).append(") ");
        }
        p.write(signature.toString());
        Set<String> writes = new HashSet<>();
        Set<String> rules = new HashSet<>();

    }
}
