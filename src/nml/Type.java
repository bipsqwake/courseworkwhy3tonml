package nml;

import Why3.Putter;

import java.io.FileReader;
import java.io.IOException;

public class Type implements Arguable{
    public static final int INT_TYPE = 0;
    public static final int BV_TYPE = 1;
    private static int tmpNum = 0;
    String name;
    String transName;
    boolean translated = false;
    private int type;
    private int bvSize = 0;

    public Type (String name, int type) {
        this.name = name; this.type = type;
    }
    public Type (String name, int type, int bvSize) {
        this.name = name;
        this.type = type;
        this.bvSize = bvSize;
    }
    public Type (String input) {
        String[] tokens = input.split("[\\(\\)]");
        if (tokens[0].equals("card")) {
            this.type = BV_TYPE;
            int tSize = 8;
            int size = Integer.parseInt(tokens[1]);
            while (size > tSize) {
                tSize *= 2;
            }
            this.bvSize = tSize;
        } else if (tokens[0].equals("int")) {
            this.type = INT_TYPE;
        }
        this.name = "tmp" + tmpNum;
        tmpNum++;
    }

    public static Type createType(Getter g) throws IOException {
        StringBuilder name = new StringBuilder();
        StringBuilder type = new StringBuilder();
        int c;
        do {                    //get name
            c = g.getChar();
        } while (Character.isWhitespace((char) c));
        while (!Character.isWhitespace((char) c)) {
            name.append((char) c);
            c = g.getChar();
        }
        while (Character.isWhitespace((char) c)) {
            c = g.getChar();
        }
        if (c != '=') { //get =
            throw new IOException("Unexpected \'=\'");
        }
        do {
            c = g.getChar();
        } while (Character.isWhitespace((char) c));
        do { //get type
            type.append((char) c);
            c = g.getChar();
        } while (c != ')');
        type.append((char) c);
        String[] types_tokens = type.toString().split("[\\(\\)]");
        if (types_tokens[0].equals("card")) {
            int size = Integer.parseInt(types_tokens[1]);
            if (size == 1) {
                return new Type(name.toString(), BV_TYPE, 1);
            }
            int tSize = 8;
            while (size > tSize) {
                tSize *= 2;
            }
            return new Type(name.toString(), BV_TYPE, tSize);
        } else if (types_tokens[0].equals("int")) {
            return new Type(name.toString(), INT_TYPE);
        } else {
            //TODO float
            return null;
        }
    }

    public String getName() {
        return name;
    }

    @Override
    public String getTransName() {return transName;}

    @Override
    public boolean isTranslated() {
        return translated;
    }

    public int getType() {
        return type;
    }

    public int getBvSize() {
        return bvSize;
    }

    public void translate(Putter p) {
        transName = this.name.toLowerCase();
        StringBuilder result = new StringBuilder();
        result.append("type ").append(transName).append(" = ");
        switch (this.type) {
            case INT_TYPE:
                break;
            case BV_TYPE:
                if (this.bvSize > 64) {
                    Log.e("Can't translate 128 yet in type " + this.name);
                    return;
                } else if (this.bvSize == 1) {
                    result.append("bool");
                } else {
                    result.append("BV").append(this.bvSize).append(".t");
                }
                break;
        }
        translated = true;
        p.write(result.toString());
    }
}
