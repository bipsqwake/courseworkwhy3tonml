package nml;

/**
 * __
 * <(o )___
 * ( ._> /
 * `---'
 */
public interface Arguable {
    public String getName();
    public String getTransName();
    public boolean isTranslated();
}
