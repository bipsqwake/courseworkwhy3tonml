package nml;

import Why3.Putter;
import nml.Expr.ProgHolder;

import java.util.List;

/**
 *   __
 * <(o )___
 * ( ._> /
 * `---'
 */
public class Mem {
    private String name;
    private String transName;
    private Type type;
    private long size;

    public String getName() {
        return name;
    }

    public Type getType() {
        return type;
    }

    public long getSize() {
        return size;
    }

    public Mem (String name, Type type, long size) {
        this.name = name;
        this.type = type;
        this.size = size;
    }

    public static Mem createMem(ProgHolder holder, Getter g) {
        StringBuilder name = new StringBuilder();
        StringBuilder type = new StringBuilder();
        char c;
        do {                    //get name
            c = g.getChar();
        } while (Character.isWhitespace((char) c));
        while (c != '[' && !Character.isWhitespace(c)) {
            name.append((char) c);
            c = g.getChar();
        }
        while (Character.isWhitespace(c)) {
            c = g.getChar();
        }
        while (c != ']') {
            type.append((char) c);
            c = g.getChar();
        }
        type.append((char) c);
        String[] type_tokens = type.toString().split("[\\[\\],]");
        Type tmpType;
        long size = 1;
        if (type_tokens.length == 2) {
            tmpType = holder.typeList.getByName(type_tokens[1]);
        } else if (type_tokens.length == 3) {
            tmpType = holder.typeList.getByName(type_tokens[2].replaceAll(" ", ""));
            size = (long)Counter.eval(type_tokens[1]);
            System.out.println(size);
        } else {return  null;}
        return new Mem(name.toString(), tmpType, size);
    }

    public void translate(Putter p) {
        transName = this.name.toLowerCase();
        StringBuilder result = new StringBuilder();
        if (size == 1) {
            result.append("val ").append(transName).append(" : ref ").append(type.transName);
        } else {
            result.append("type ").append(transName).append("_type").append(" = {mutable ");
            result.append(transName).append("_mem : array ").append(type.transName).append("}");
            result.append("invariant { length self.").append(transName).append("_mem = ").append(size).append("}\n");
            result.append("val ").append(transName).append(" : ").append(transName).append("_type");
        }
        p.write(result.toString());
    }
}
