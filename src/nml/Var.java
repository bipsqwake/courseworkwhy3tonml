package nml;

import Why3.Putter;
import nml.Expr.ProgHolder;

import java.util.List;

/**
 *   __
 * <(o )___
 * ( ._> /
 * `---'
 */
public class Var {
    private String name;
    private Type type;
    private String transName;

    public Var(String name, Type type) {
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public Type getType() {
        return type;
    }

    public static Var createVar(ProgHolder holder, Getter g) {
        StringBuilder name = new StringBuilder();
        StringBuilder type = new StringBuilder();
        char c;
        do {                    //get name
            c = g.getChar();
        } while (Character.isWhitespace((char) c));
        while (c != '[' && !Character.isWhitespace(c)) {
            name.append((char) c);
            c = g.getChar();
        }
        while (Character.isWhitespace(c)) {
            c = g.getChar();
        }
        while (c != ']') {
            type.append((char) c);
            c = g.getChar();
        }
        type.append((char) c);
        String[] type_tokens = type.toString().split("[\\[\\], ]");
        if (type_tokens.length != 2) {
            return null;
        }
        Type tmpType = holder.typeList.getByName(type_tokens[1]);
        return new Var(name.toString(), tmpType);
    }

    public void translate(Putter p) {
        if (!type.translated) {
            Log.e("Cant translate " + name + " cause of not translated type");
            return;
        }
        StringBuilder result = new StringBuilder();
        transName = name.toLowerCase();
        result.append("val ").append(transName).append(" : ref ").append(type.transName);
        p.write(result.toString());
    }
}
