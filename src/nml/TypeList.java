package nml;

import java.util.LinkedList;
import java.util.List;

public class TypeList {
    List<Type> types = new LinkedList<>();

    public void add (Type type) {
        types.add(type);
    }
    public Type getByName(String name) {
        for (Type a : types) {
            if (a.name.equals(name)) {
                return a;
            }
        }
        Type tmp = new Type(name);
        for (Type a: types) {
            if (tmp.getName().equals(a.getName()) && tmp.getBvSize() == a.getBvSize() && tmp.getType() == a.getType()) {
                return a;
            }
        }
        add(tmp);
        return tmp;
    }

    public List<Type> getList() {
        return types;
    }
}
