import Why3.Putter;
import nml.Log;
import nml.Model;

import java.io.IOException;

/**
 *   __
 * <(o )___
 *  ( ._> /
 *   `---'
 */
public class Application {
    public static String[] filename = {"powerpc.nml", "powerpc_alu.nml"};

    public static void main(String[] args) {
        Model model = new Model();
        Log.init();
        for (String a: filename) {
            try {
                model.process(a);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Putter putter = new Putter("out.mlw", "header.txt");
        model.translate(putter);
        putter.end("ender.txt");
        model.out();
    }
}
