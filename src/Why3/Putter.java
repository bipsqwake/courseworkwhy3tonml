package Why3;

import nml.Log;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 *   __
 * <(o )___
 * ( ._> /
 * `---'
 */
public class Putter {
    FileWriter writer;

    public Putter(String filename, String header) {
        try {
            this.writer = new FileWriter(filename);
            FileReader reader = new FileReader(header);
            StringBuilder head = new StringBuilder();
            int c;
            while ((c = reader.read()) != -1) {
                head.append((char)c );
            }
            writer.write(head.toString());
            writer.flush();
        } catch (IOException e) {
            Log.e("Cant open file to write or header");
        }
    }

    public void write(String string) {
        try {
            writer.write(string + "\n");
            writer.flush();
        } catch (IOException e) {
            Log.e("Can't put string - " + string);
        }
    }

    public void end(String ender) {
        try {
            FileReader reader = new FileReader(ender);
            StringBuilder end = new StringBuilder();
            int c;
            while ((c = reader.read()) != -1) {
                end.append((char)c );
            }
            writer.write(end.toString());
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
